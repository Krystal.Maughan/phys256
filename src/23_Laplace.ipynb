{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Physics 256\n",
    "## Solving Laplace's Equation\n",
    "\n",
    "\\begin{eqnarray}\n",
    "\\nabla \\cdot \\vec{E} &= \\frac{\\rho}{\\varepsilon_0} \\quad&\\quad \\nabla \\times \\vec{E} &= -\\frac{\\partial\\vec{B}}{\\partial t} \\newline\n",
    "\\nabla \\cdot \\vec{B} &= 0 \\quad&\\quad \\nabla \\times \\vec{B} &= \\mu_0 \\left(\\vec{J} +\\varepsilon_0 \\frac{\\partial\\vec{E}}{\\partial t}\\right)\n",
    "\\end{eqnarray}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import style\n",
    "style._set_css_style('../include/bootstrap.css')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Last Time\n",
    "\n",
    "### [Notebook Link: 22 FFT](./22_FFT.ipynb)\n",
    "\n",
    "- Fast Fourier transform\n",
    "- Using the FFT to analyze chaos\n",
    "\n",
    "## Today\n",
    "\n",
    "- Elliptical differential equations\n",
    "- Spatial discretization and the Laplace equation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up the Notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "%matplotlib inline\n",
    "plt.style.use('../include/notebook.mplstyle');\n",
    "%config InlineBackend.figure_format = 'svg'\n",
    "colors = plt.rcParams['axes.prop_cycle'].by_key()['color']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Laplace's Equation\n",
    "\n",
    "In the absence of any charge density ($\\rho=0$) the scalar electric potential is related to the electric field via:\n",
    "\n",
    "\\begin{equation}\n",
    "\\vec{E} = -\\nabla V\n",
    "\\end{equation}\n",
    "\n",
    "and thus the first of Maxwell's equation is the Laplace equation for the scalar potential:\n",
    "\n",
    "\\begin{equation}\n",
    "\\nabla^2 V = 0.\n",
    "\\end{equation}\n",
    "\n",
    "In three spatial dimensions this has the form of an *elliptic* partial differential equation:\n",
    "\n",
    "\\begin{equation}\n",
    "\\frac{\\partial^2}{\\partial x^2} V(x,y,z) + \\frac{\\partial^2}{\\partial y^2} V(x,y,z) + \\frac{\\partial^2}{\\partial z^2} V(x,y,z) = 0.\n",
    "\\end{equation}\n",
    "\n",
    "This equation is very different than the ordinary differential equations (ODE) we have solved thus far in this class.  Here, we have a 2nd order partial differential equation (PDE) where we know the *boundary conditions*.  Unlike for the case of ODEs, there is no single class of systematic integrators which differ only by their accuracy.  Instead, we have to determine the best algorithm on a case-by-case basis.  For elliptical PDEs, we will study **relaxation** methods which work well.\n",
    "\n",
    "## Discretization \n",
    "In analogy to our approach for ODEs (where we discretized time) we will discretize space by writing:\n",
    "\n",
    "\\begin{align}\n",
    "x_i &= i \\Delta x \\newline\n",
    "y_i &= i \\Delta y \\newline\n",
    "z_i &= i \\Delta z \n",
    "\\end{align}\n",
    "\n",
    "where $\\Delta x, \\Delta y, \\Delta z \\ll 1$ and we define:\n",
    "\n",
    "\\begin{equation}\n",
    "V(x_i,y_j,z_k) = V_{ijk} = V(i,j,k). \n",
    "\\end{equation}\n",
    "\n",
    "Our first step is to write the Laplace equation as a *difference* equation.\n",
    "\n",
    "### Forward Derivative\n",
    "We have already used this discrete approximation to the deriviative derived from the Taylor expansion near $x_i$:\n",
    "\n",
    "\\begin{equation}\n",
    "\\frac{\\partial V}{\\partial x}  \\approx \\frac{V(i+1,j,k) - V(i,j,k)}{\\Delta x}.\n",
    "\\end{equation}\n",
    "\n",
    "\n",
    "### Backwards Derivative\n",
    "We could also have expanded in the oppositie direction which would give:\n",
    "\n",
    "\\begin{equation}\n",
    "\\frac{\\partial V}{\\partial x}  \\approx \\frac{V(i,j,k) - V(i-1,j,k)}{\\Delta x}.\n",
    "\\end{equation}\n",
    "\n",
    "### Centered Derivative\n",
    "Let's combine these two approaches.  Consider the Taylor expansion for a function $f$ of a single variable $x$:\n",
    "\n",
    "\\begin{align}\n",
    "f(x+\\Delta x) &= f(x) + f'(x) \\Delta x + \\frac{1}{2} f''(x) (\\Delta x)^2 + \\frac{1}{6} f'''(x)(\\Delta x)^3 + \\cdots \\newline\n",
    "f(x-\\Delta x) &= f(x) - f'(x) \\Delta x + \\frac{1}{2} f''(x) (\\Delta x)^2 - \\frac{1}{6} f'''(x)(\\Delta x)^3 + \\cdots \n",
    "\\end{align}\n",
    "\n",
    "Subtracting these two expressions yields:\n",
    "\n",
    "\\begin{align}\n",
    "f(x+\\Delta x) - f(x-\\Delta x) &= 2 f'(x) \\Delta x + \\frac{1}{3} f'''(x) (\\Delta x)^3 \\newline \n",
    "\\Rightarrow f'(x) &= \\frac{d f}{dx} = \\frac{f(x+\\Delta x) - f(x-\\Delta x)}{2 \\Delta x} + \\mathrm{O}(\\Delta x^2) .\n",
    "\\end{align}\n",
    "\n",
    "This is the centered derivative and it is accurate to order $\\Delta x^2$ as opposed to order $\\Delta x$ for the forward and backward derivatives.\n",
    "\n",
    "### 2nd Derivative\n",
    "If we added instead of subtracting we would have found:\n",
    "\n",
    "\\begin{align}\n",
    "f(x+\\Delta x) + f(x-\\Delta x) &= 2 f(x)  + f''(x) (\\Delta x)^2 \\newline \n",
    "\\Rightarrow f''(x) &= \\frac{d^2 f}{dx^2} = \\frac{f(x+\\Delta x) + f(x-\\Delta x) - 2f(x)}{(\\Delta x)^2} + \\mathrm{O}(\\Delta x^2) .\n",
    "\\end{align}\n",
    "\n",
    "We can think of this as the combination of a forward and backward derivative at step $\\Delta x/2$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"span alert alert-success\">\n",
    "<h2> Programming challenge </h2>\n",
    "\n",
    "Consider the function $f(x) = \\ln x$. \n",
    "<ol>\n",
    "<li> Compare the forward and centered derivative of $f(x)$ on $x \\in [2,3]$ with the exact result using $\\Delta x = 0.1$.</li>\n",
    "<li> Compare the 2nd derivative of $f(x)$ on $x \\in [2,3]$ with the exact result. </li>\n",
    "</ol>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    return np.log(x)\n",
    "\n",
    "def df(f,x):\n",
    "    '''Compute the forward, centered and 2nd derivative of f = ln(x)'''\n",
    "    Δx = x[1]-x[0]\n",
    "        \n",
    "    dff,dcf,d2f = 0,0,0\n",
    "    pass\n",
    "    \n",
    "    return dff,dcf,d2f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "N = 10\n",
    "x = np.linspace(2,3,N)\n",
    "dff,dcf,d2f = df(f,x)\n",
    "\n",
    "fig, axes = plt.subplots(1,2,sharex=True, sharey=False, squeeze=True, figsize=(9,4))\n",
    "fig.subplots_adjust(wspace=0.5)\n",
    "\n",
    "axes[0].plot(x,1/x, lw=1.5, label=r'$1/x$')\n",
    "axes[0].plot(x,dff,'s', mfc='None', ms=5, label='Forward Deriv.')\n",
    "axes[0].plot(x,dcf,'o', mfc='None', ms=5, label='Centered Deriv.')\n",
    "\n",
    "axes[0].set_ylabel(\"f'(x)\")\n",
    "axes[0].set_xlabel('x')\n",
    "axes[0].legend(handlelength=1)\n",
    "axes[0].set_xlim(2,3)\n",
    "\n",
    "axes[1].plot(x,-1/(x*x), lw=1.5, label=r'$-1/x^2$')\n",
    "axes[1].plot(x,d2f,'o', mfc='None', ms=5, label='2nd Centered Deriv.')\n",
    "axes[1].set_xlabel('x')\n",
    "axes[1].set_ylabel(\"f''(x)\")\n",
    "axes[1].legend(loc='lower right', handlelength=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Discretizing the Laplace Equation\n",
    "\n",
    "We can now apply our centered 2nd derivative to the Laplace equation:\n",
    "\n",
    "\\begin{align}\n",
    "\\frac{\\partial^2}{\\partial x^2} V(x,y,z) + \\frac{\\partial^2}{\\partial y^2} V(x,y,z) + \\frac{\\partial^2}{\\partial z^2} V(x,y,z) &\\approx  \\frac{1}{(\\Delta x)^2}\\left[V(i+1,j,k)+V(i-1,j,k) - 2V(i,j,k)\\right] \\newline\n",
    "&+ \\; \\frac{1}{(\\Delta y)^2}\\left[V(i,j+1,k)+V(i,j-1,k) - 2V(i,j,k)\\right] \\newline\n",
    "&+ \\; \\frac{1}{(\\Delta z)^2}\\left[V(i,j,k+1)+V(i,j,k-1) - 2V(i,j,k)\\right] \\newline\n",
    "&= 0\n",
    "\\end{align}\n",
    "\n",
    "and if we assume a cubic grid where $\\Delta x = \\Delta y = \\Delta z = \\Delta$ we can rearrange to find:\n",
    "\n",
    "\\begin{equation}\n",
    "V(i,j,k) = \\frac{1}{6} \\left[V(i+1,j,k) + V(i-1,j,k) + V(i,j+1,k) + V(i,j-1,k) + V(i,j,k+1) + V(i,j,k-1) \\right].\n",
    "\\end{equation}\n",
    "\n",
    "The solution to the Laplace equation (subject to some boundary conditions) is that this expression is satisfied at **all** grid points.\n",
    "\n",
    "### Question: What does this physically imply about the solution?\n",
    "\n",
    "## Relaxation Methods\n",
    "\n",
    "How do we construct a numerical solution to this problem?  We need to know $V$ at all of its neighbors, so we can't just start at the known boundary.  \n",
    "\n",
    "The answer is that we simply **guess** an initial configuration from the field (constant, or random in practice) and iterate this expression until we find a stable solution.  Such *relaxation* methods are self-consistent as they use the latest version of the solution as input.\n",
    "\n",
    "\\begin{equation}\n",
    "V_0(i,j,k) \\to V_1(i,j,k) \\to V_2(i,j,k) \\to \\dots\n",
    "\\end{equation}\n",
    "\n",
    "We continue this process until some level of convergence has been reached as measured by the size of the change after iteration. This iterative procedure places an effective *time* index on our Laplace equation, i.e. $V_n$ where $t = n \\Delta t$.  We require that the long-time steady state solution of our time-dependent equation is the solution of our original Laplace equation.  Consider a function $\\overline{V}(x,y,z,t)$ which satisfies:\n",
    "\n",
    "\\begin{equation}\n",
    "\\frac{\\partial}{\\partial t} \\overline{V}(x,y,z,t) = D \\left(\\frac{\\partial^2}{\\partial x^2} \\overline{V}(x,y,z,t) + \\frac{\\partial^2}{\\partial y^2} \\overline{V}(x,y,z,t) + \\frac{\\partial^2}{\\partial z^2} \\overline{V}(x,y,z,t) \\right)\n",
    "\\end{equation}\n",
    "\n",
    "This is simply the **diffusion equation** and our Laplace equation is the equilibrium or steady state limit: $V(x,y,z) \\equiv \\overline{V}(x,y,z,t\\to\\infty)$.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
